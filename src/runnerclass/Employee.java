/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package runnerclass;

/**
 *
 * @author cristian.palencia001
 */
public class Employee {
        private int years;
    
    public Employee(int initialYears) {
        years = initialYears;
    }
    
    public int getYears() {
        return years;
    }
    
     public int getHours() {
        return 40;           // works 40 hours / week
    }
    
    public double getSalary() {
        return 50000.0;      // $40,000.00 / year
    }
    
    public int getVacationDays() {
        return 10 + getSeniorityBonus();          // 2 weeks' paid vacation
    }

    public String getVacationForm() {
        return "yellow";     // use the yellow form
    }
    
    public int getSeniorityBonus() {
        return 2 * years;
    }

}
