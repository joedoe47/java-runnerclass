/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package runnerclass;

/**
 *
 * @author cristian.palencia001
 */
public class Lawyer extends Employee{
    
    public Lawyer(int years) {
	        super(years);  // calls Employee constructor
	    }

    
    @Override
    public String getVacationForm() {
	        return "pink";
	    }
    
    @Override
        public int getVacationDays() {
        return super.getVacationDays() + 5;
    }

     
       public void sue() {
        System.out.println("I'll see you in court!");
    }
       
       
      public double getSalary() {
        return super.getSalary() + 5000 * getYears();
    }

    
    
}
