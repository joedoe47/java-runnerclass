/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package runnerclass;

/**
 *
 * @author cristian.palencia001
 */
public class Secretary extends Employee {
    
    public Secretary(int years) {
        super(years);
    }
    
    @Override
     public int getSeniorityBonus() {
        return 0;
    }


           public void takeDictation(String text) {
        System.out.println("Taking dictation of text: " + text);
    }
}
