/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package runnerclass;

/**
 *
 * @author cristian.palencia001
 */
public class LegalSecretary extends Secretary {
    
    public LegalSecretary(int years){
        super(years);
    }
    
    public void fileLegalBriefs() {
        System.out.println("I could file all day!");
    }

    @Override
    public double getSalary() {
        return 45000.0;      // $45,000.00 / year
    }

    
}
