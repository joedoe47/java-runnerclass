/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package runnerclass;

import java.util.ArrayList;

/**
 *
 * @author cristian.palencia001
 */
public class RunnerClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Employee> myEmployees= new ArrayList<Employee>();
        myEmployees.add(new Secretary(5));
        myEmployees.add(new Marketer(6));
        myEmployees.add(new Lawyer(12));
        myEmployees.add(new LegalSecretary(3));
        myEmployees.add(new Lawyer(20));
        
        
        for(Employee p: myEmployees)
        {
            printInfo(p);
        }
        
    }
    public static void printInfo(Employee empl) {
        System.out.println("salary: " + empl.getSalary());
        System.out.println("v.days: " + empl.getVacationDays());
        System.out.println("v.form: " + empl.getVacationForm());
        System.out.println();
    }
}
